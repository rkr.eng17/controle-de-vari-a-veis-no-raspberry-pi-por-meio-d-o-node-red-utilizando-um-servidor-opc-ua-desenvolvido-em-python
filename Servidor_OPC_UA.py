from opcua import Server
from time import sleep
import os
os.system('cls' if os.name == 'nt' else 'clear')

ip_OPC = '192.168.0.146'
porta_OPC = '4840'

def main():
    servidor = Server()
    servidor.set_endpoint(f'opc.tcp://{ip_OPC}:{porta_OPC}')

    spaceNome = 'Flexsim'
    space = servidor.register_namespace(spaceNome)

    objetos = servidor.get_objects_node()
    grupo_objetos = objetos.add_object(space, 'Grupo Objetos')

    estado01 = grupo_objetos.add_variable(space, 'GPIO 4', 0)
    estado02 = grupo_objetos.add_variable(space, 'GPIO 17', 1)

    
    estado01.set_writable()
    estado02.set_writable()

    
    servidor.start()

    try:
        x1 = estado01.get_value()
        x2 = estado02.get_value()
   
        count = 1
        while True:
            estado_atual_01 = estado01.get_value()
            estado_atual_02 = estado02.get_value()
         
 
            if count == 1: 
                os.system('cls' if os.name == 'nt' else 'clear')
                print(f'Estado 01: {estado_atual_01}')
                print(f'Estado 02: {estado_atual_02}')

                count = 0
            if estado_atual_01 != x1 :
                os.system('cls' if os.name == 'nt' else 'clear')
                print(f'Estado 01: {estado_atual_01}')
                print(f'Estado 02: {estado_atual_02}')

                x1 = estado_atual_01
                                    
            if estado_atual_02 != x2 :
                os.system('cls' if os.name == 'nt' else 'clear')
                print(f'Estado 01: {estado_atual_01}')
                print(f'Estado 02: {estado_atual_02}')
                x2 = estado_atual_02
            
    finally:
        servidor.stop()

if _name_ == "_main_":
    main()